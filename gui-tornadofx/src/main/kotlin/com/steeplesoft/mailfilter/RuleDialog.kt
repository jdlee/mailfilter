package com.steeplesoft.mailfilter

import com.steeplesoft.mailfilter.model.Rule
import com.steeplesoft.mailfilter.model.RuleType
import javafx.beans.Observable
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.ButtonBar
import javafx.scene.control.ButtonType
import javafx.scene.control.ComboBox
import javafx.scene.control.Dialog
import javafx.scene.control.ListView
import javafx.scene.control.MultipleSelectionModel
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import tornadofx.*

class RuleDialog(val rule : Rule = Rule()) : Dialog<Rule?>() {
    private lateinit var ruleType: ComboBox<String> 
    private lateinit var sourceFolder: TextField 
    private lateinit var destinationFolder: TextField 
    private lateinit var age: NumberField
    private lateinit var matchingText: TextArea
    private lateinit var fields: ListView<String>

    init {
        dialogPane.buttonTypes.addAll(ButtonType.OK, ButtonType.CANCEL)
        setResultConverter { dialogButton ->
            if (dialogButton.buttonData == ButtonBar.ButtonData.OK_DONE) {
                rule
            } else {
                null
            }
        }

        dialogPane.content = buildView()

    }

    private fun buildView(): Node {
        return gridpane {
            vgap = 2.0
            row {
                label("Rule Details") {
                    addClass(MailfilterStyle.sectionHeader)
                    alignment = Pos.CENTER_LEFT
                }
            }
            row {
                label("Type")
                combobox<String> {
                    items = FXCollections.observableArrayList(RuleType.MOVE.name, RuleType.DELETE.name)
                    selectionModel.select(items.indexOf(rule.type?.name ?: RuleType.MOVE.name))
                    ruleType = this
                    selectionModel.selectedItemProperty().addListener { _, _, newValue ->
                        rule.setType(newValue)
                    }
                }
            }
            row {
                label("Source Folder")
                textfield {
                    sourceFolder = this
                    text = rule.sourceFolder
                    textProperty().addListener { _, _, newValue ->
                        rule.sourceFolder = newValue
                    }

                }
            }
            row {
                label("Destination Folder")
                textfield {
                    destinationFolder = this
                    text = rule.destFolder
                    textProperty().addListener { _, _, newValue ->
                        rule.destFolder = newValue
                    }
                }
            }
            row {
                label("Age")
                numberfield {
                    age = this
                    text = rule.olderThan?.toString()
                    textProperty().addListener { _, _, newValue ->
                        if (!newValue.isNotBlank()) {
                            rule.olderThan = newValue.toInt()
                        }
                    }
                }
            }
            row {
                label("Matching Text")
                textarea {
                    matchingText = this
                    text = rule.matchingText.joinToString("\n").trim()
                    textProperty().addListener { _, _, newValue ->
                        rule.matchingText = newValue.trim().split("\n") as MutableList<String>
                    }
                }
            }
            row {
                label("Fields") {
                    alignment = Pos.TOP_LEFT
                }
                listview(FXCollections.observableArrayList("from", "to", "cc", "subject", "body")) {
                    fields = this
                    multiSelect(true)
                    selectionModel.selectedItems.addListener { o : Observable  ->
                            rule.fields = (o as Iterable<String>).toMutableSet()
                    }
                }
            }
        }
    }
}
