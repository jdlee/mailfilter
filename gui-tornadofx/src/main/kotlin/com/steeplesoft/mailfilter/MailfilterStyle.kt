package com.steeplesoft.mailfilter

import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class MailfilterStyle : Stylesheet() {
    companion object {
        val sectionHeader by cssclass()
    }

    init {
        sectionHeader {
            fontSize = 15.px
            fontWeight = FontWeight.BOLD
        }
    }
}
