package com.steeplesoft.mailfilter

import com.steeplesoft.mailfilter.model.Account
import com.steeplesoft.mailfilter.model.Rule
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.event.EventTarget
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Alert
import javafx.scene.control.ListView
import javafx.scene.control.TableView
import javafx.scene.layout.Priority
import tornadofx.*
import javafx.scene.control.ButtonType
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCombination
import java.util.Optional



class MailFilterView : View() {
    val controller: MailFilterController by inject()
    private var ruleTable: TableView<Rule> by singleAssign()
    private var accountList: ListView<Account> by singleAssign()

    override val root = borderpane {
        prefWidth = 1200.0
        prefHeight = 900.0
        top {
            menubar {
                menu("_File") {
                    item("_Save") {
                        accelerator = KeyCombination.valueOf("Ctrl-S")
                        action {
                            controller.saveAccounts()
                        }
                    }
                    item("_Run") {
                        accelerator = KeyCombination.valueOf("F5")
                        action {
                            controller.runRules(accountList.selectionModel.selectedItems)
                        }
                    }
                    item("E_xit") {
                        action {
                            if (confirmSaveBeforeClose()) {
                                Platform.exit();
                            }
                        }
                    }
                }
            }
        }
        left {
            vbox {
                padding = Insets(10.0)
                sectionHeaderButtonBar("Accounts", {
                    controller.addAccount()
                }, {
                    val account = accountList.selectedItem
                    account?.let {
                        controller.removeAccount(account)
                    }
                })
                accountList = listview(controller.accounts) {
                    vgrow = Priority.ALWAYS
                    cellFormat { text = it.userName }
                    setOnMouseClicked {
                        val account = accountList.selectionModel.selectedItem
                        account?.let {
                            controller.selectAccount(account)
                            ruleTable.items = controller.rules
                        }
                    }
                }
            }
        }
        center {
            vbox {
                padding = Insets(10.0)
                sectionHeaderButtonBar("Rules",
                        {
                            controller.addRule()
                        },
                        {
                            val rule = ruleTable.selectedItem
                            rule?.let {
                                controller.removeRule(rule)
                            }
                        },
                        {
                            button {
                                action {
                                    println("foo")
                                }
                                text = "Hello"
                            }
                        }
                )
                ruleTable = tableview(controller.rules) {
                    vgrow = Priority.ALWAYS
                    hgrow = Priority.ALWAYS
                    isEditable = false
                    column("Type", Rule::type)
                    column("Source", Rule::sourceFolder)
                    column("Description", Rule::description) {
                        runLater {
                            val width = this.tableView.columns.map { it.width }.sum()
                            prefWidth = this.tableView.width - width
                        }
                        hgrow = Priority.ALWAYS
                    }
                    onDoubleClick {
                        val rule = selectionModel.selectedItem
                        rule?.let {
                            controller.editRule(rule)
                        }
                    }
                }
            }
        }
    }

    init {
        primaryStage.isMaximized = true;
        primaryStage.setOnCloseRequest { event ->
            if (!confirmSaveBeforeClose()) {
                event.consume();
            }
        }
    }

    private fun confirmSaveBeforeClose() : Boolean {
        val alert = Alert(Alert.AlertType.CONFIRMATION);
        alert.title = "Confirmation Dialog"
        alert.contentText = "Save before closing?"
        alert.buttonTypes.setAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL)

        val result = alert.showAndWait()
        val buttonType = result.get()
        if (buttonType == ButtonType.YES) {
            controller.saveAccounts()
        } else if (buttonType == ButtonType.CANCEL) {
            return false;
        }

        return true;
    }

    private fun EventTarget.sectionHeaderButtonBar(label: String,
                                                   addMethod: () -> Unit,
                                                   removeMethod: () -> Unit,
                                                   extra: () -> Unit = {} ) {
        addChildIfPossible(
                borderpane {
                    padding = Insets(0.0, 0.0, 5.0, 0.0)
                    left {
                        label(label) {
                            addClass(MailfilterStyle.sectionHeader)
                        }
                    }
                    right {
                        buttonbar {
                            button {
                                action {
                                    addMethod()
                                }
                                text = "+"

                            }
                            button {
                                action {
                                    removeMethod()
                                }
                                text = "-"
                            }
                            extra()
                        }
                    }
                }
        )
    }
}
