package com.steeplesoft.mailfilter

import com.steeplesoft.mailfilter.model.Account
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.ButtonBar
import javafx.scene.control.ButtonType
import javafx.scene.control.CheckBox
import javafx.scene.control.Dialog
import javafx.scene.control.PasswordField
import javafx.scene.control.TextField
import tornadofx.addClass
import tornadofx.checkbox
import tornadofx.gridpane
import tornadofx.label
import tornadofx.passwordfield
import tornadofx.row
import tornadofx.textfield

class AccountDialog : Dialog<Account?>() {
    private lateinit var serverName : TextField
    private lateinit var serverPort : NumberField
    private lateinit var userName : TextField
    private lateinit var password : PasswordField
    private lateinit var useSsl : CheckBox

    init {
        dialogPane.buttonTypes.addAll(ButtonType.OK, ButtonType.CANCEL)
        setResultConverter { dialogButton ->
            if (dialogButton.buttonData == ButtonBar.ButtonData.OK_DONE) {
                getAccount()
            } else {
                null
            }
        }

        dialogPane.content = buildView()
    }

    private fun getAccount() : Account {
        val account = Account()
        account.serverName = serverName.text
        account.serverPort = serverPort.number
        account.userName = userName.text
        account.password = password.text
        account.isUseSsl = useSsl.isSelected
        return account
    }

    private fun buildView(): Node {
        return gridpane {
            vgap = 2.0
            row {
                label("Account Details") {
                    addClass(MailfilterStyle.sectionHeader)
                    alignment = Pos.CENTER_LEFT
                }
            }
            row {
                label("Server")
                textfield {
                    serverName = this
                }
            }
            row {
                label("Server Port")
                numberfield {
                    serverPort = this
                }
            }
            row {
                label("User Name")
                textfield {
                    userName = this
                }
            }
            row {
                label("Password")
                passwordfield {
                    password = this
                }
            }
            row {
                label("Use SSL")
                checkbox  {
                    useSsl = this
                }
            }
        }
    }
}
