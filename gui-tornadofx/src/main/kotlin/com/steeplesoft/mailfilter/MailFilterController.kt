package com.steeplesoft.mailfilter

import com.steeplesoft.mailfilter.model.Account
import com.steeplesoft.mailfilter.model.Rule
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import tornadofx.Controller

class MailFilterController : Controller() {
    private val accountService = AccountService();
    var accounts: ObservableList<Account>
    var rules = FXCollections.observableArrayList<Rule>()!!

    init {
        accounts = FXCollections.observableList(accountService.accounts)
    }

    fun addAccount() {
        val result = AccountDialog().showAndWait()
        if (result.isPresent) {
            accounts.add(result.get())
        }
    }

    fun removeAccount(account : Account) {
        accounts.remove(account)
    }

    fun addRule() {
        val result = RuleDialog().showAndWait()
        if (result.isPresent) {
            rules.add(result.get())
        }
    }

    fun editRule(rule : Rule) {
        val result = RuleDialog(rule).showAndWait()
//        if (result.isPresent) {
//            rules.add(result.get())
//        }
    }

    fun removeRule(rule : Rule) {
        rules.remove(rule)
    }

    fun selectAccount(account: Account) {
        rules = FXCollections.observableList(account.rules)
    }

    fun saveAccounts() {
        accountService.saveAccounts(accounts)
    }

    fun runRules(selectedItems: List<Account>?) {
        selectedItems?.forEach {
            println(it.userName)
        }
    }
}

