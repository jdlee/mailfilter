package com.steeplesoft.mailfilter

import javafx.event.EventTarget
import javafx.scene.control.TextField
import tornadofx.addChildIfPossible

class NumberField : TextField() {
    val number : Int?
        get() {
            text?.let {
                return if (!text.isNullOrBlank()) text.toInt() else null
            }
            return null
        }
    init {
        textProperty().addListener { _, oldValue, newValue ->
            newValue?.let {
                text = if (newValue.matches(Regex("^[0-9]*\\.?[0-9]*$"))) newValue else oldValue
            }
        }
    }


}

fun EventTarget.numberfield(value: Int? = null, op: NumberField.() -> Unit = {}) {
    val node = NumberField()
    addChildIfPossible(node)
    op(node)
    if (value != null) {
        node.text = value.toString()
    }
}