package com.steeplesoft.mailfilter.test

import com.steeplesoft.mailfilter.AccountService
import com.steeplesoft.mailfilter.model.Account
import com.steeplesoft.mailfilter.model.Rule
import com.steeplesoft.mailfilter.model.RuleType
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

import java.io.File
import java.io.IOException
import java.util.ArrayList
import java.util.HashSet
import java.util.logging.Level
import java.util.logging.Logger

/**
 *
 * @author jason
 */
class AccountServiceTest {

    @Test
    fun withBadFileName() {
        Assertions.assertThrows(IllegalArgumentException::class.java) { AccountService("") }
    }

    @Test
    fun withNoName() {
        Assertions.assertThrows(IllegalArgumentException::class.java) { AccountService(null) }
    }

    @Test
    fun goodFile() {
        val accountService = AccountService("src/test/resources/rules.json")
        val accounts = accountService.accounts
        Assert.assertTrue(accounts!!.isNotEmpty())
    }

    @Test
    fun writeAccounts() {
        val fields = mutableSetOf("to", "from", "cc")

        val account = Account()
        account.serverName = "test"
        account.serverPort = 49152
        account.userName = "test@example.com"
        account.password = "password"

        val accounts = ArrayList<Account>()
        accounts.add(account)

        val rules = ArrayList<Rule>()
        rules.add(Rule(sourceFolder = "Some box",
                type = RuleType.DELETE,
                olderThan = 45,
                fields = fields))

        account.rules = rules

        val tempFile = File.createTempFile("rules", "json")
        val service = AccountService(tempFile.canonicalPath)
        service.saveAccounts(accounts)

        val read = service.accounts
        Assert.assertEquals(accounts, read)
    }
}
