package com.steeplesoft.mailfilter.test

import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.GreenMailUtil
import com.icegreen.greenmail.util.ServerSetup
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger
import javax.mail.Folder
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.internet.AddressException
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

/**
 *
 * @author jason
 */
class TestMailUtil private constructor() {
    private val greenMail: GreenMail = GreenMail(ServerSetup.ALL)

    init {
        greenMail.start()
    }

    fun createTestMessages() {
        try {
            val user = greenMail.setUser(TEST_RECIP, "password")

            val subject = GreenMailUtil.random()
            val body = GreenMailUtil.random()
            user.deliver(createMimeMessage("themaster@timelords.com", subject, body, null))
            user.deliver(createMimeMessage("ad1@adco.net", "Great stuff cheap!", "body", null))
            user.deliver(createMimeMessage("newsletter@spam.com", "Happening this month", "stuff", null))
            user.deliver(createMimeMessage("pmp@training.net", "Get certified today!", "Cheap!", null))
            user.deliver(createMimeMessage("joe.blow@blargh.net", "It's been a long time!", "Let's talk", null))
            user.deliver(createMimeMessage("themaster@timelords.com", "The Sound of Drums", "boom boom boom",
                    Date.from(LocalDate.now().minusYears(2).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())))

            val imapSession = greenMail.imap.createSession()
            val store = imapSession.getStore("imap")
            store.connect(TEST_RECIP, "password")

            val defaultFolder = store.defaultFolder
            val inbox = store.getFolder("INBOX")
            inbox.open(Folder.READ_WRITE)

            val ads = defaultFolder.getFolder("Ads")
            ads.create(Folder.HOLDS_MESSAGES)

            val spam = defaultFolder.getFolder("Spam")
            spam.create(Folder.HOLDS_MESSAGES)
        } catch (ex: MessagingException) {
            Logger.getLogger(TestMailUtil::class.java.name).log(Level.SEVERE, null, ex)
        }

    }

    @Throws(AddressException::class, MessagingException::class)
    private fun createMimeMessage(from: String, subject: String, body: String, date: Date?): MimeMessage {
        val msg = MimeMessage(greenMail.smtp.createSession())
        msg.setFrom(InternetAddress(from))
        msg.addRecipient(Message.RecipientType.TO,
                InternetAddress(TEST_RECIP))
        msg.subject = subject
        msg.setText(body)
        msg.sentDate = date ?: Date()
        return msg
    }

    companion object {
        private val TEST_RECIP = "to@localhost.com"
        private var INSTANCE: TestMailUtil? = null

        fun instance(): TestMailUtil {
            if (INSTANCE == null) {
                INSTANCE = TestMailUtil()
            }
            return INSTANCE!!
        }
    }
}
