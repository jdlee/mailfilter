/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.mailfilter.test

import com.steeplesoft.mailfilter.model.Rule
import com.steeplesoft.mailfilter.model.RuleType
import com.steeplesoft.mailfilter.model.validation.ValidRule
import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import org.junit.Assert
import org.junit.jupiter.api.Test

/**
 *
 * @author jason
 */
class ValidRuleValidatorTest {

    private val validator = Validation.buildDefaultValidatorFactory().validator

    @Test
    fun emptyRule() {
        val rule = Rule()
        val violations = validator.validate(rule)
        Assert.assertFalse(violations.isEmpty())
    }

    @Test
    fun matchingRuleMustSpecifyDestination() {
        val rule = Rule()
        rule.matchingText = mutableListOf("dummy")
        val violations = validator.validate(rule)
        Assert.assertFalse(violations.isEmpty())
    }

    @Test
    fun matchingRuleMustSpecifyFields() {
        val rule = Rule()
        rule.matchingText = mutableListOf("dummy")
        rule.destFolder = "TRASH"
        val violations = validator.validate(rule)
        Assert.assertFalse(violations.isEmpty())
    }

    @Test
    fun moveMessageByMatchingText() {
        val rule = Rule()
        rule.matchingText = mutableListOf("dummy")
        rule.destFolder = "TRASH"
        rule.fields.add("from")
        val violations = validator.validate(rule)
        Assert.assertTrue(violations.isEmpty())
    }

    @Test
    fun moveMessageByAge() {
        val rule = Rule()
        rule.destFolder = "TRASH"
        rule.olderThan = 30
        val violations = validator.validate(rule)
        Assert.assertTrue(violations.isEmpty())
    }

    @Test
    fun deleteMessageByMatchingText() {
        val rule = Rule()
        rule.setType("delete")
        rule.matchingText = mutableListOf("dummy")
        rule.fields.add("from")
        val violations = validator.validate(rule)
        Assert.assertTrue(violations.isEmpty())
    }

    @Test
    fun deleteMessageByAge() {
        val rule = Rule()
        rule.setType("delete")
        rule.olderThan = 30
        val violations = validator.validate(rule)
        Assert.assertTrue(violations.isEmpty())
    }

    @Test
    fun deleteMessageWithInvalidAge() {
        val rule = Rule()
        rule.setType("delete")
        rule.olderThan = -1
        val violations = validator.validate(rule)
        Assert.assertFalse(violations.isEmpty())
    }

    @Test
    fun validRuleShouldOnlyWorkOnRules() {
        val violations = validator.validate(NotARule())
        Assert.assertFalse(violations.isEmpty())
    }

    @ValidRule
    private inner class NotARule
}
