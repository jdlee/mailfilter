/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.mailfilter.test

import com.steeplesoft.mailfilter.model.Rule
import java.util.HashSet
import javax.mail.Message
import javax.mail.search.FromStringTerm
import javax.mail.search.OrTerm
import javax.mail.search.RecipientStringTerm
import javax.mail.search.SearchTerm

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 *
 * @author jason
 */
class RuleTest {

    @Test
    fun multipleFieldsShouldReturnOrTerm() {
        val fields = mutableSetOf("to","from","cc")

        val rule = Rule(matchingText = mutableListOf("testText"), fields = fields)
        val term = rule.searchTerm
        Assertions.assertTrue(term is OrTerm)
        val terms = (term as OrTerm).terms
        for (t in terms) {
            Assertions.assertTrue(t is FromStringTerm
                    || t is RecipientStringTerm && t.recipientType === Message.RecipientType.CC
                    || t is RecipientStringTerm && t.recipientType === Message.RecipientType.TO)
        }
    }
}
