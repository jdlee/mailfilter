package com.steeplesoft.mailfilter.exceptions

import com.steeplesoft.mailfilter.model.Rule
import javax.validation.ConstraintViolation

/**
 *
 * @author jason
 */
class RuleValidationException(private val violations: Set<ConstraintViolation<Rule>>) : MailFilterException() {
    override val message: String?
        get() : String? {
            val message = StringBuilder("Rule validation error(s) occurred:")
            for (violation in violations) {
                message.append("\n\t").append(violation.message)
            }

            return message.toString()
        }
}
