package com.steeplesoft.mailfilter.exceptions

/**
 *
 * @author jason
 */
open class MailFilterException : RuntimeException()
