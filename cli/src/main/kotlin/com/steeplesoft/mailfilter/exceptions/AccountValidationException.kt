package com.steeplesoft.mailfilter.exceptions

import com.steeplesoft.mailfilter.model.Account
import javax.validation.ConstraintViolation

/**
 *
 * @author jason
 */
class AccountValidationException(private val violations: Set<ConstraintViolation<Account>>) : java.lang.RuntimeException() {
    override val message: String?
        get() : String? {
            val message = StringBuilder("Account validation error(s) occurred:")
            for (violation in violations) {
                message.append("\n\t").append(violation.message)
            }

            return message.toString()
        }
}
