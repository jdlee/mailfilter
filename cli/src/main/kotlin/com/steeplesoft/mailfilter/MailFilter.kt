package com.steeplesoft.mailfilter

import java.io.File
import java.util.logging.Level
import java.util.logging.Logger
import javax.mail.MessagingException

class MailFilter {
    var moved: Int = 0
        private set
    var deleted: Int = 0
        private set
    private val fileName: String?

    constructor() : this(null) {
    }

    constructor(fileName: String? = null) {
        this.fileName = fileName
    }

    fun run() {
        try {
            val service = AccountService(fileName)

            for (account in service.accounts!!) {
                val processor = AccountProcessor(account)
                println("Running rules for ${account.userName}...")
                processor.process()
                deleted += processor.deleteCount
                moved += processor.moveCount
            }
        } catch (ex: MessagingException) {
            Logger.getLogger(MailFilter::class.java.name).log(Level.SEVERE, null, ex)
        }

    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            try {
                val mailFilter = MailFilter(if (args.isNotEmpty()) args[1] else null)
                mailFilter.run()
                println("\tDeleted count: " + mailFilter.deleted)
                println("\tMove count:    " + mailFilter.moved)
            } catch (e: Exception) {
                System.err.println(e.localizedMessage)
            }

        }
    }

}
