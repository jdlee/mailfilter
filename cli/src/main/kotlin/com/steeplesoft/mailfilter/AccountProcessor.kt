package com.steeplesoft.mailfilter

import com.steeplesoft.mailfilter.model.Account
import com.steeplesoft.mailfilter.model.Rule
import com.steeplesoft.mailfilter.model.RuleType
import java.util.Arrays
import java.util.HashMap
import java.util.Properties
import java.util.logging.Level
import java.util.logging.Logger
import javax.mail.Flags
import javax.mail.Folder
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.NoSuchProviderException
import javax.mail.Session
import javax.mail.Store

/**
 *
 * @author jdlee
 */
class AccountProcessor @Throws(MessagingException::class)
constructor(private val account: Account) {
    private val folders : MutableMap<String, Folder> = HashMap()
    var deleteCount: Int = 0
        private set
    var moveCount: Int = 0
        private set
    private var store: Store? = null

    @Throws(MessagingException::class)
    fun process() {
        try {
            getImapSession()

            for ((key, value) in getRulesByFolder(account.rules)) {
                processFolder(key, value)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            throw RuntimeException(e)
        } finally {
            closeFolders()
            store?.close()
        }
    }

    @Throws(MessagingException::class, NoSuchProviderException::class)
    private fun getImapSession() {
        val props = Properties()
        props["mail.imap.ssl.trust"] = "*"
        props["mail.imaps.ssl.trust"] = "*"
        props.setProperty("mail.imap.starttls.enable", java.lang.Boolean.toString(account.isUseSsl))
        val session = Session.getInstance(props, null)
        store = session.getStore(if (account.isUseSsl) "imaps" else "imap")
        store?.connect(account.serverName, account.userName, account.password)
    }

    @Throws(MessagingException::class)
    private fun processFolder(folder: String, rules: List<Rule>) {
        val messages = getFolder(folder, Folder.READ_WRITE).messages
        val set = mutableSetOf<Message>()
                set.addAll(messages)
        Arrays.stream(messages)
//                .parallel()
                .forEach { message ->
//                    println ("Processing ${message.subject} from ${message.from.first()} in ${message.folder}\r")
                    rules.stream()
                            .filter { rule -> messageMatches(rule, message) }
                            .forEach { rule ->
                                when (rule.type) {
                                    RuleType.MOVE -> moveMessage(message, getFolder(rule.destFolder!!, Folder.READ_WRITE))
                                    RuleType.DELETE -> deleteMessage(message)
                                }
                            }
                }
    }

    private fun messageMatches(rule: Rule, message: Message?) : Boolean {
        return rule.searchTerm!!.match(message)
    }

    private fun getRulesByFolder(rules: List<Rule>): Map<String, List<Rule>> {
        return rules.groupBy { it.sourceFolder }
//        return rules.stream().collect<Map<String, List<Rule>>, Any>(
//                Collectors.groupingBy<Rule, String, Any, List<Rule>>({ r -> r.sourceFolder },
//                        Collectors.toList()))
    }

    private fun deleteMessage(toDelete: Message?) {
        toDelete?.let {
            try {
                val source = toDelete.folder
                println ("Deleting ${toDelete.subject}' from ${toDelete.from.first()} on ${toDelete.receivedDate}")
                source.setFlags(arrayOf(toDelete), FLAGS_DELETED, true)
                deleteCount++
            } catch (ex: MessagingException) {
                throw RuntimeException(ex)
            }
        }
    }

    private fun moveMessage(toMove: Message?, dest: Folder) {
        toMove?.let {
            try {
                val source = toMove.folder
                val messages = arrayOf(toMove)
                println ("Moving '${toMove.subject}' from ${toMove.from.first()} on ${toMove.receivedDate} from ${source} to ${dest.name}:")
                source.setFlags(messages, FLAGS_DELETED, true)
                source.copyMessages(messages, dest)
                moveCount++
            } catch (ex: MessagingException) {
                throw RuntimeException(ex)
            }

        }
    }

    private fun getFolder(folderName: String, mode: Int): Folder {
        var source: Folder? = null
        try {
            if (folders.containsKey(folderName)) {
                source = folders[folderName]
            } else {
                source = store!!.getFolder(folderName)
                if (source == null || !source.exists()) {
                    throw IllegalArgumentException("Invalid folder: $folderName")
                }
                folders[folderName] = source
                source.open(mode)
            }
        } catch (ex: MessagingException) {
            Logger.getLogger(AccountProcessor::class.java.name).log(Level.SEVERE, null, ex)
        }

        return source!!
    }

    private fun closeFolders() {
        folders.values.stream()
                .filter { f -> f.isOpen }
                .forEachOrdered { f ->
                    try {
                        f.close(true)
                    } catch (e: MessagingException) {
                    }
                }
    }

    companion object {
        private val FLAGS_DELETED = Flags(Flags.Flag.DELETED)
    }
}
