package com.steeplesoft.mailfilter

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.steeplesoft.mailfilter.exceptions.AccountValidationException
import com.steeplesoft.mailfilter.exceptions.RuleValidationException
import com.steeplesoft.mailfilter.model.Account
import com.steeplesoft.mailfilter.model.Rule
import java.io.File
import java.io.IOException
import java.util.HashSet
import java.util.logging.Level
import java.util.logging.Logger
import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator

/**
 *
 * @author jason
 */
class AccountService @JvmOverloads constructor(fileName: String? = null) {
    private val DEFAULT_RULE_FILE_NAME = (System.getProperty("user.home") + File.separatorChar
            + ".mailfilter" + File.separatorChar + "rules.json")
    private val rulesFile: File

    val accounts: List<Account>?
        get() {
            val validator = Validation.buildDefaultValidatorFactory().validator
            val mapper = ObjectMapper()
                    .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
            var accounts: List<Account>? = null
            try {
                accounts = mapper.readValue<List<Account>>(rulesFile,
                        object : TypeReference<List<Account>>() {

                        })
                accounts?.forEach { account ->
                    val accountViolations = validator.validate(account)
                    if (accountViolations.size > 0) {
                        throw AccountValidationException(accountViolations)
                    }
                    account.rules.sortWith(Comparator { o1, o2 -> o1.type!!.compareTo(o2.type!!) })
                }
            } catch (ex: IOException) {
                Logger.getLogger(AccountService::class.java.name).log(Level.SEVERE, null, ex)
            }

            return accounts
        }

    init {
        this.rulesFile = getRulesFile(fileName)
    }

    fun saveAccounts(accounts: List<Account>) {
        try {
            ObjectMapper()
                    .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
                    .writerWithDefaultPrettyPrinter()
                    .writeValue(rulesFile, accounts)
        } catch (ex: IOException) {
            Logger.getLogger(AccountService::class.java.name).log(Level.SEVERE, null, ex)
        }

    }

    private fun getRulesFile(fileName: String?): File {
        val file = File(fileName ?: DEFAULT_RULE_FILE_NAME)
        if (!file.exists()) {
            file.parentFile.mkdirs()
            file.createNewFile()
            file.writeText("[]")
        }
        return file
    }
}

class BackedObservableList<T>