/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.mailfilter.model

import com.steeplesoft.mailfilter.model.validation.ValidRule
import java.util.ArrayList
import java.util.Objects
import javax.validation.constraints.Min
import org.hibernate.validator.constraints.NotBlank

/**
 *
 * @author jdlee
 */
data class Account(@NotBlank(message = "A value must be specified for userName")
                   var userName: String? = null,
                   @NotBlank(message = "A value must be specified for password")
                   var password: String? = null,
                   @NotBlank(message = "A value must be specified for serverName")
                   var serverName: String? = null) {


    @Min(value = 0L, message = "The value must be positive")
    var serverPort: Int? = 993
    var isUseSsl = true
    @ValidRule
    var rules: MutableList<Rule> = ArrayList()
}
