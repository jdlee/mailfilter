package com.steeplesoft.mailfilter.model.validation

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

/**
 *
 * @author jason
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.FIELD)
@Retention
@Constraint(validatedBy = [(ValidRuleValidator::class)])
@MustBeDocumented
annotation class ValidRule(val message: String = "Validation errors",
                           val groups: Array<KClass<*>> = [],
                           val payload: Array<KClass<out Payload>> = [])