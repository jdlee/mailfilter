package com.steeplesoft.mailfilter.model

/**
 *
 * @author jason
 */
enum class RuleType {
    DELETE, MOVE;

    companion object {
        fun getRuleType(type: String?): RuleType {
            return when (type?.toLowerCase()) {
                "delete" -> DELETE
                "move" -> MOVE
                else -> throw RuntimeException("Invalid rule type")
            }
        }
    }
}
