package com.steeplesoft.mailfilter.model.validation

import com.steeplesoft.mailfilter.model.Rule
import com.steeplesoft.mailfilter.model.RuleType
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

/**
 *
 * @author jason
 */
class ValidRuleValidator : ConstraintValidator<ValidRule, Any> {
    override fun initialize(constraintAnnotation: ValidRule) {}

    override fun isValid(value: Any?, ctx: ConstraintValidatorContext): Boolean {
        if (value == null) {
            addViolation(ctx, "Null values are not considered valid Rules")
            return false
        }
        var valid = true
        if (value is Collection<*>) {
            for (o in value) {
                valid = valid and validateRule(o!!, ctx)
            }
        } else {
            valid = validateRule(value, ctx)
        }
        return valid
    }

    private fun validateRule(value: Any, ctx: ConstraintValidatorContext): Boolean {
        if (value !is Rule) {
            addViolation(ctx, "Constraint valid only on instances of Rule.")
            return false
        }
        var valid = true

        if (value.type == RuleType.MOVE) {
            valid = valid and validateNotBlank(ctx, value.destFolder, "A destination folder must be specified.")
        }
        if (value.matchingText.isNotEmpty()) {
            valid = valid and validateFields(ctx, value)
        } else if (value.olderThan != null) {
            if (value.olderThan!! <= 0) {
                addViolation(ctx, "The age must be greater than 0.")
                valid = false
            }
        } else if (value.olderThan == null) {
            addViolation(ctx, "Either matchingText or olderThan must be specified.")
            valid = false
        }

        return valid
    }

    private fun addViolation(ctx: ConstraintValidatorContext, message: String) {
        ctx.disableDefaultConstraintViolation()
        ctx.buildConstraintViolationWithTemplate(message)
                .addConstraintViolation()
    }

    private fun validateFields(ctx: ConstraintValidatorContext, rule: Rule): Boolean {
        if (rule.fields.isEmpty()) {
            addViolation(ctx, "Rules which specify a matching text must specify the field(s) to match on.")
            return false
        }

        return true
    }

    private fun validateNotBlank(ctx: ConstraintValidatorContext, value: String?, message: String): Boolean {
        if (isBlank(value)) {
            addViolation(ctx, message)
            return false
        }
        return true
    }

    private fun isBlank(value: String?): Boolean {
        return value?.trim { it <= ' ' }?.isEmpty() ?: true
    }
}
