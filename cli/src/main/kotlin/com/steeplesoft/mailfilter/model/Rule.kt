package com.steeplesoft.mailfilter.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.steeplesoft.mailfilter.model.validation.ValidRule
import org.hibernate.validator.constraints.NotBlank
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date
import java.util.stream.Collector
import java.util.stream.Collectors
import javax.mail.Message
import javax.mail.search.BodyTerm
import javax.mail.search.ComparisonTerm
import javax.mail.search.FromStringTerm
import javax.mail.search.OrTerm
import javax.mail.search.RecipientStringTerm
import javax.mail.search.SearchTerm
import javax.mail.search.SentDateTerm
import javax.mail.search.StringTerm
import javax.mail.search.SubjectTerm
import javax.validation.constraints.Min

/**
 *
 * @author jdlee
 */
@ValidRule
data class Rule(var type : RuleType? = RuleType.MOVE,
                @NotBlank(message = "Rules must specify a source folder.")
                var sourceFolder: String = "INBOX",
                var destFolder : String? = null,
                var matchingText : MutableList<String> = mutableListOf(),
                var fields : MutableSet<String> = HashSet(),
                @Min(value = 1L, message = "The age must be greater than 0.")
                var olderThan : Int? = null) {

    private var term: SearchTerm? = null
    var description : String
        @JsonIgnore
        get() {
            return when (type) {
                RuleType.MOVE ->
                        if (matchingText.isNotEmpty()) {
                            String.format("Move emails matching '%s' to '%s'", matchingText, destFolder)
                        } else {
                            String.format("Move emails older than %s to '%s'", olderThan, destFolder)
                        }
                RuleType.DELETE ->
                        if (matchingText.isNotEmpty()) {
                            String.format("Delete emails matching '%s'", matchingText)
                        } else {
                            String.format("Delete emails older than %s", olderThan)
                        }
                else -> {
                    ""
                }
            }
        }
        set(desc ) = Unit // Required for JavaFX's use

    val searchTerm: SearchTerm?
        @JsonIgnore
        get() {
            if (term == null) {
                if (matchingText.isNotEmpty()) {
                    val terms = fields.map { f -> createFieldSearchTerm(f) }
                    term = OrTerm(terms.toTypedArray())
                } else if (olderThan != null) {
                    val day = LocalDateTime.now().minusDays(olderThan!!.toLong())
                    term = SentDateTerm(ComparisonTerm.LE,
                            Date.from(day.toLocalDate().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()))
                }
            }

            return term
        }

    fun setType(type: String?) {
        type?.let {
            this.type = RuleType.getRuleType(type)
        }
    }

    private fun createFieldSearchTerm(f: String): SearchTerm? {
        return when (f.toLowerCase()) {
            "from" -> createMatchingTextTermList { FromStringTerm(it) }
            "cc" -> createMatchingTextTermList { RecipientStringTerm(Message.RecipientType.CC ,it) }
            "to" -> createMatchingTextTermList { RecipientStringTerm(Message.RecipientType.TO, it) }
            "body" -> createMatchingTextTermList { BodyTerm(it) }
            "subject" -> createMatchingTextTermList { SubjectTerm(it) }
            else -> null
        }
    }

    private fun createMatchingTextTermList(builder: (String) -> StringTerm): OrTerm {
        return OrTerm(matchingText.stream()
                .map {
                    builder(it)
                }
                .collect(Collectors.toList())
                .toTypedArray())
    }

    companion object {

        fun create(): Rule {
            return Rule()
        }
    }

}
